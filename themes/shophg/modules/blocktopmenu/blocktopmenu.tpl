{if $MENU != ''}
<!-- Menu -->
<div id="block_top_menu" class="sf-contener clearfix col-lg-12">
	<div class="cat-title">{l s="Menu" mod="blocktopmenu"}</div>
	<ul class="sf-menu clearfix menu-content">


		<li>
			<a href="/4-katalog">{l s="Wszystkie produkty" mod="blocktopmenu"}</a>
		</li>

		<li class="sfHover">
			<a href="/4-katalog">{l s="Kolor" mod="blocktopmenu"}</a>
			<ul style="display: none;" class="submenu-container clearfix">
				<li>
					<span class="square-20 square-20-blue"></span>
					<a href="/4-katalog#/color-blue">
						<span>{l s="Niebieski" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-brown"></span>
					<a href="/4-katalog#/color-brown">
						<span>{l s="Brązowy" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-grey"></span>
					<a href="/4-katalog#/color-grey">
						<span>{l s="Szary" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-green"></span>
					<a href="/4-katalog#/color-green">
						<span>{l s="Zielony" mod="blocktopmenu"}</span>
					</a>
				</li>
<!-- 				<li>
					<span class="square-20 square-20-heather"></span>
					<a href="/4-katalog#/color-heather">
						<span>{l s="Wszos" mod="blocktopmenu"}</span>
					</a>
				</li> -->
				<li>
					<span class="square-20 square-20-orange"></span>
					<a href="/4-katalog#/color-orange">
						<span>{l s="Pomarańczowy" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-pink"></span>
					<a href="/4-katalog#/color-pink">
						<span>{l s="Różowy" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-purple"></span>
					<a href="/4-katalog#/color-purple">
						<span>{l s="Fioletowy" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-red"></span>
					<a href="/4-katalog#/color-red">
						<span>{l s="Czerwony" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-white"></span>
					<a href="/4-katalog#/color-white">
						<span>{l s="Biały" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-yellow"></span>
					<a href="/4-katalog#/color-yellow">
						<span>{l s="Żółty" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<span class="square-20 square-20-black"></span>
					<a href="/4-katalog#/color-black">
						<span>{l s="Czarny" mod="blocktopmenu"}</span>
					</a>
				</li>
			</ul>
		</li>



		<li class="sfHover">
			<a href="/4-katalog">{l s="Kategorie" mod="blocktopmenu"}</a>
			<ul style="display: none;" class="submenu-container clearfix">
				<li>
					<a href="/4-katalog#/category-t_shirt">
						<span>{l s="T-Shirty" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-poloshirt">
						<span>{l s="Koszulki polo" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-corporate_wear">
						<span>{l s="Odzież korporacyjna" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-sweatshirt">
						<span>{l s="Bluzy" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-polar_softshell">
						<span>{l s="Polary / softshelle" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-sport">
						<span>{l s="Odzież sportowa" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-windbreaker">
						<span>{l s="Wiatrówki" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-bodywarmer">
						<span>{l s="Bezrękawniki" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-jacket">
						<span>{l s="Kurtki" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-kid">
						<span>{l s="Odzież dziecięca" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-underwear">
						<span>{l s="Bielizna" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-towel">
						<span>{l s="Szlafroki i ręczniki" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-cap">
						<span>{l s="Czapki i akcesoria" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li>
					<a href="/4-katalog#/category-bag">
						<span>{l s="Torby" mod="blocktopmenu"}</span>
					</a>
				</li>
			</ul>
		</li>









		<li class="sfHover">
			<a href="/4-katalog">{l s="Płeć" mod="blocktopmenu"}</a>
			<ul style="display: none;" class="submenu-container clearfix">
				<li class="level1 nav-4-0 first">
					<a href="/4-katalog#/gender-children_s_clothing">
						<span>{l s="Ubrania dziecięce" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li class="level1 nav-4-1">
					<a href="/4-katalog#/gender-women_s_clothing">
						<span>{l s="Ubrania Damskie" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li class="level1 nav-4-2">
					<a href="/4-katalog#/gender-men_s_clothing">
						<span>{l s="Ubrania męskie" mod="blocktopmenu"}</span>
					</a>
				</li>
				<li class="level1 nav-4-3 last">
					<a href="/4-katalog#/gender-unisex_clothing">
						<span>{l s="Unisex Odzież" mod="blocktopmenu"}</span>
					</a>
				</li>
			</ul>
		</li>






		{$MENU}
		{if $MENU_SEARCH}
		<li class="sf-search noBack" style="float:right">
			<form id="searchbox" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" method="get">
				<p>
					<input type="hidden" name="controller" value="search" />
					<input type="hidden" value="position" name="orderby"/>
					<input type="hidden" value="desc" name="orderway"/>
					<input type="text" name="search_query" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}" />
				</p>
			</form>
		</li>
		{/if}
	</ul>
</div>
<!--/ Menu -->
{/if}